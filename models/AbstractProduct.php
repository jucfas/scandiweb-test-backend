<?php

namespace Models\Products;

abstract class AbstractProduct
{
    private $data;
    private $table;
    private $db;
    private $properties;
    public function __construct($data, $db)
    {
        $this->data = $data;
        $this->table = "products";
        $this->db = $db;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getDatabase()
    {
        return $this->db;
    }

    // Define type-specific properties
    abstract public function getProperties();
// Check if JSON has required data
    public function hasProperties()
    {
        // Properties for all products
        $requiredAll = array("name", "sku", "price", "type");
        $required = array_merge($this->getProperties(), $requiredAll);
        $hasProperties = true;
        foreach ($required as $property) {
            if (property_exists($this->data, $property) === false) {
                $hasProperties = false;
                break;
            }
        }

        return $hasProperties;
    }

    abstract public function setData();
}
