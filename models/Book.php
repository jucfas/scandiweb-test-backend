<?php

namespace Models\Products;

class Book extends AbstractProduct
{
    public function setData()
    {
        $data = $this->getData();
        $data->height = null;
        $data->width = null;
        $data->length = null;
        $data->size = null;
        $data->dimensions = null;
        $data->unit = "KG";
        $data->attribute = "weight";
        $this->getDatabase()->create($data, $this->getTable());
    }

    public function getProperties()
    {
         return array("weight");
    }
}
