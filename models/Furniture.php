<?php

namespace Models\Products;

class Furniture extends AbstractProduct
{
    public function getDimensions($data)
    {
        // Returns dimensions as HxWxL
        $dimensions = array($data->height, $data->width, $data->length);
        return implode("x", $dimensions);
    }

    public function setData()
    {
        $data = $this->getData();
        $data->size = null;
        $data->weight = null;
        $data->dimensions = $this->getDimensions($data);
        $data->unit = "CM";
        $data->attribute = "dimensions";
        $this->getDatabase()->create($data, $this->getTable());
    }

    public function getProperties()
    {
        return array("height", "width", "length");
    }
}
