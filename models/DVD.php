<?php

namespace Models\Products;

class Dvd extends AbstractProduct
{
    public function setData()
    {
        $data = $this->getData();
        $data->height = null;
        $data->width = null;
        $data->length = null;
        $data->weight = null;
        $data->dimensions = null;
        $data->unit = "MB";
        $data->attribute = "size";
        $this->getDatabase()->create($data, $this->getTable());
    }

    public function getProperties()
    {
        return array("size");
    }
}
