<?php

namespace Config\Database;

use PDO;

class MySQL extends Database implements DatabaseInterface
{
    // DB Params
    private $host = "localhost";
    private $dbName = "products";
    private $username = "root";
    private $password = "1234";
    private $conn;

    protected function __construct()
    {
        try {
            $this->conn = new PDO(
                "mysql:host=" . $this->host . ";dbname=" . $this->dbName,
                $this->username,
                $this->password
            );
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection Error: " . $e->getMessage();
        }
    }

    // Connect to DB
    public function getConnection()
    {
        return $this->conn;
    }

    public function read($tableName)
    {
        // Read All
        $query = 'SELECT * FROM ' . $tableName;
        // execute a query
        $statement = $this->getConnection()->query($query);
        // fetch all rows
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $response = array();
        $response["data"] = $result;
        echo json_encode($response);
    }

    public function create($data, $tableName)
    {
        $query = "INSERT INTO " . $tableName . " (name, sku, price, type, size, dimensions, weight, unit, attribute)
        VALUES (
            '$data->name',
            '$data->sku',
            '$data->price',
            '$data->type',
            '$data->size',
            '$data->dimensions',
            '$data->weight',
            '$data->unit',
            '$data->attribute')";

        $this->getConnection()->exec($query);
    }

    public function delete($tableName, $ids)
    {
        $idsStr = implode(", ", $ids);
        $query = "DELETE FROM $tableName WHERE id IN ($idsStr)";
        $this->getConnection()->exec($query);

        echo json_encode(array("message" => "The records of IDs ($idsStr) were deleted successfully."));
    }
}
