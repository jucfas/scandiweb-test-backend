<?php

namespace Config\Database;

interface DatabaseInterface
{
    public function getConnection();
}
