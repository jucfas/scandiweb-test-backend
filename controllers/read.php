<?php

// Headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

use Config\Database\{MySQL};

require_once "../config/Database.php";
require_once "../config/DatabaseInterface.php";
require_once "../config/MySQL.php";

$db = MySQL::getInstance();

$db->read('products');
