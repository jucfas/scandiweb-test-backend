<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

use Config\Database\{MySQL};

require_once "../config/Database.php";
require_once "../config/DatabaseInterface.php";
require_once "../config/MySQL.php";

$json = file_get_contents("php://input");
$data = json_decode($json);
$table = "products";
$ids = $data->products;

$db = MySQL::getInstance();

$db->delete($table, $ids);
