<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

require_once "../config/Database.php";
require_once "../config/DatabaseInterface.php";
require_once "../config/MySQL.php";
require_once "../models/AbstractProduct.php";
require_once "../models/DVD.php";
require_once "../models/Furniture.php";
require_once "../models/Book.php";

$json = file_get_contents("php://input");
$jsonData = json_decode($json);
if ($jsonData !== null) {
    $type = strtolower($jsonData->type);
} else {
    $type = null;
}

$db = Config\Database\MySQL::getInstance();

$types = array("dvd", "furniture", "book");

if ($type !== null && in_array($type, $types)) {
    $className = "Models\Products\\" . ucfirst($type);
    $product = new $className($jsonData, $db);

    // Check if data contains properties
    if ($product->hasProperties()) {
        $product->setData();
        echo json_encode(array("message" => "The record was created successfully."));
    } else {
        echo json_encode(array("message" => "The record was not created: not all the 
        required properties for this type have been submitted."));
    }
} else {
    echo json_encode(array("message" => "The submitted data does not contain a valid product type."));
}
